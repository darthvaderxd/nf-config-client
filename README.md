# nf-config-client

NilFactor Config Client is a client designed to be used with nf-config. It allows you to easily load config settings from the server, so you can easily manage your services configuration.

```
//...
var token = require('nf-config-client');
var database = token.loadConfig('database-dev')
    .then((db) => {
        config.database = db;
    })
    .catch((error) => {
        console.log(error);
    });
//...
```

# Environment Variables to note
```
CONFIG_CLIENT_ROUTE="config/get"
CONFIG_CLIENT_API_KEY="apikey"
CONFIG_CLIENT_API_TOKEN="token"
CONFIG_CLIENT_URL="http://localhost:8080"
```

If you use the nf-config server you don't have to worry about changing CONFIG_CLIENT_ROUTE, however if there was a similar config server where the get route was similar to pat/config/*name* you could change the CONFIG_CLIENT_ROUTE to match that config server and use it.