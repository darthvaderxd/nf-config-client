'use strict';

const rc = require('restify-clients');
const route = process.env.CONFIG_CLIENT_ROUTE || 'config/get';
const key = process.env.CONFIG_CLIENT_API_KEY || 'apikey';
const token = process.env.CONFIG_CLIENT_API_TOKEN || 'token';
const baseUrl = process.env.CONFIG_CLIENT_URL || 'http://localhost:8080';

module.exports.loadConfig = (configName) => {
    console.log(baseUrl);
    var client = rc.createJsonClient({
        url: baseUrl
    });

    var params = {
        path: `/${route}/${configName}`,
        headers: {}
    };

    params.headers[key] = token;

    console.log(params);
    return new Promise((resolve, reject) => {
        client.get(params, (error, request, response, data) => {
            if (error) {
                reject(err);
            } else {
                resolve(data.result);
            }
        });
    });
};
